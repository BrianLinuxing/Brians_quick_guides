# Brians_quick_guides

A list of handy notes, which might help others.

But no guarantees, these are very rough and more like aide-mémoire than proper documentation (which I might occasionally write on one of my blogs). Use at your own peril!

Your mileage will vary, particularly if the related software changes, is updated or functionality is removed or enhanced.

Use these notes if you please but don’t complain to me if they don’t work or break something. 